<?php
/**
 * @file
 * Admin page callbacks and functionality for Comment Edited module
 */

/**
 * Add functionality to rewrite the last comment message
 */
function comment_edited_admin_page() {
  $form = array();
  $default = comment_edited_default_message();
  $form['comment_edited_template'] = array(
    '#type' => 'textfield',
    '#default_value' => variable_get('comment_edited_template', $default),
    '#title' => t('Default message'),
    '#description' => t('This message will be used to display below comments.'),
  );
  return $form;
}
